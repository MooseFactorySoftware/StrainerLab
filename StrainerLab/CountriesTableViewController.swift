//
//  CountriesTableViewController.swift
//  StrainerLab
//
//  Created by Tristan Leblanc on 31/05/2018.
//  Copyright © 2018 MooseFactory Software. All rights reserved.
//

import UIKit
import Strainer

class CountriesTableViewController: UITableViewController {
	
	/// countriesData
	///
	/// The source objects
	
	lazy var countriesData: [Country] = {
		if let jsonURL = Bundle.main.url(forResource: "Countries", withExtension: "json")  {
			do {
				let data = try Data(contentsOf: jsonURL)
				let decoder = JSONDecoder()
				let countries = try decoder.decode([Country].self, from: data)
				return countries
			}
			catch {
				print("Can't read countries file - \(error)")
			}
		}
		return [Country]()
	}()

	/// countries
	///
	/// The countries objects provider.
	
	lazy var countries: Strainer.Provider<Country> = {
		let provider = Strainer.Provider<Country>(objects: self.countriesData)
		return provider
	}()

	/// sortByNameFilter
	///
	/// The filter that sorts countries by name.
	
	lazy var sortByNameFilter: Sorter = {
		Sorter(input: countries, function: Sorter.sortByName)
	}()

	/// sortByRankFilter
	///
	/// The filter that sorts countries by rank.
	
	lazy var sortByRankFilter: Sorter = {
		Sorter(input: sortByNameFilter, function: Sorter.sortByRank)
	}()
	
	/// scopeFilter
	///
	/// The filter that filters countries depending on selected search scope
	
	lazy var scopeFilter: ScopeFilter = {
		ScopeFilter(input: sortByRankFilter, function: ScopeFilter.scopeFilter)
	}()
	
	/// searchFilter
	///
	/// The filter that filters countries depending on searched string
	
	lazy var searchFilter: SearchFilter = {
		SearchFilter(input: scopeFilter, function: SearchFilter.containsString)
	}()
	
	/// Some display constants
	
	let hilightAttribute = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 17), NSAttributedStringKey.foregroundColor : UIColor.blue]
	
	enum RankColor: Int {
		case main
		case common
		case lessCommon
		case uncommon
		
		var color: UIColor {
			switch self {
			case .main:
				return UIColor.red.withAlphaComponent(0.3)
			case .common:
				return UIColor.red.withAlphaComponent(0.17)
			case .lessCommon:
				return UIColor.red.withAlphaComponent(0.1)
			case .uncommon:
				return UIColor.red.withAlphaComponent(0.05)
			}
		}
	}
	
    // MARK: - Table view data source
	override func viewDidLoad() {
		sortByNameFilter.active = true
		sortByRankFilter.active = false
		scopeFilter.scope = .world
		searchFilter.searchString = nil
		countries.refresh(completion: { _ in
			self.tableView.reloadData()
		})
	}
}

extension CountriesTableViewController {
	
    override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return countries.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath)
		if let country = countries[indexPath.row] {
			cell.textLabel?.text = country.name
			cell.detailTextLabel?.text = country.code
			cell.contentView.backgroundColor = RankColor(rawValue: country.rank)?.color ?? UIColor.white
			highlightSearch(label: cell.textLabel, attributes: hilightAttribute)
			highlightSearch(label: cell.detailTextLabel, attributes: hilightAttribute)
		}
        return cell
    }

	func highlightSearch(label: UILabel?, attributes:[NSAttributedStringKey : Any]) {
		guard let label = label, let text = label.text, let search = searchFilter.searchString else {
			return
		}
		if text.count > 0 {
			let atrText = NSMutableAttributedString(string: text)
			let searchRange = NSString(string: text).range(of: search, options: [.caseInsensitive])
			atrText.setAttributes(attributes, range: searchRange )
			label.attributedText = atrText
			return
		}
		label.attributedText = NSAttributedString(string: text)
	}

}

extension CountriesTableViewController : UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		searchFilter.searchString = searchText
		countries.refresh(completion: { changes in
			self.updateChanges(changes)
		})
	}
	
	func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
		scopeFilter.scope = ScopeFilter.Scope(rawValue: selectedScope) ?? .world
		countries.refresh(completion: { changes in
			self.updateChanges(changes)
		})
	}
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchBar.resignFirstResponder()
	}
}

extension CountriesTableViewController {
	
	@IBAction func sortByRankSwitchChanged(sender: UISwitch) {
		sortByRankFilter.active = sender.isOn
		countries.refresh(completion: { changes in
			self.updateChanges(changes)
		})
	}
}
