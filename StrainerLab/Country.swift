//
//  Country.swift
//  StrainerLab
//
//  Created by Tristan Leblanc on 31/05/2018.
//  Copyright © 2018 MooseFactory Software. All rights reserved.
//

import Foundation
import Strainer

// {"us_name": "Belgium","code": "BE","rank": 1,"Schengen": 1,"EU": 1}

struct Country : Codable {
	let code: String
	let name: String
	let schengen: Bool
	let europe: Bool
	let rank: Int
	
	enum CodingKeys: String, CodingKey {
		case code
		case name = "us_name"
		case schengen = "Schengen"
		case europe = "EU"
		case rank
	}
	
}

extension Country: KeyedObject {
	var key: ObjectKey { return ObjectKey.string(code) }
}
