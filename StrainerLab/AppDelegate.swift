//
//  AppDelegate.swift
//  StrainerLab
//
//  Created by Tristan Leblanc on 31/05/2018.
//  Copyright © 2018 MooseFactory Software. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

}

