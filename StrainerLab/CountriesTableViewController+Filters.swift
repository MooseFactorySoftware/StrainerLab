//
//  CountriesTableViewController+Filters.swift
//  StrainerLab
//
//  Created by Tristan Leblanc on 31/05/2018.
//  Copyright © 2018 MooseFactory Software. All rights reserved.
//

import Foundation
import Strainer

extension CountriesTableViewController {

	class Sorter : Strainer.Filter<Country> {
		
		static let sortByRank = Strainer.Function<Country>.sortFunction({ (items, _) -> [Country] in
			return items.sorted(by: { (lhl,lhr) in
				if lhl.rank == lhr.rank {
					return lhl.name < lhr.name
				}
				return lhl.rank < lhr.rank })
		})
		
		static let sortByName = Strainer.Function<Country>.sortFunction({ (items, _) -> [Country] in
			return items.sorted(by: { (lhl,lhr) in lhl.name < lhr.name })
		})
		
	}
	
	class ScopeFilter : Strainer.Filter<Country> {
		
		enum Scope: Int {
			case world
			case europe
			case schengen
			
			static let key = "Scope"
			
		}
		
		var scope: Scope = .world { didSet {
			changed = true
			shouldClearCache = true
			context = [Scope.key: scope]
			}}
		
		static let scopeFilter = Strainer.Function<Country>.filterFunction({ (items, context) -> [Country] in
			let scope = context?[Scope.key] as? Scope ?? .world
			switch scope {
			case .world:
				return items
			case .europe:
				return items.filter{ $0.europe }
			case .schengen:
				return items.filter{ $0.schengen }
			}
		})
		
	}
	
	class SearchFilter : Strainer.TextSearchFilter<Country> {
		
		static let containsString = Strainer.Function<Country>.filterFunction({ (items, context) -> [Country]? in
			guard let string = context?[Strainer.SearchStringKey] as? String else {
				return nil
			}
			return items.filter{ $0.name.lowercased().contains(string) || $0.code.lowercased().contains(string) }
		})
		
	}
}
