![StrainerLogo](StrainerLogo.jpg)

# StrainerLab

**Strainer Demonstration Application**

This app demonstrates the usage of the Strainer framework

<https://gitlab.com/MooseFactorySoftware/StrainerLab>

**Strainer** is an object provider / sorting / filtering framework.
It can be found here:
<https://gitlab.com/MooseFactorySoftware/Strainer>

**Strainer** is also available through CocoaPods. To install it, simply add the following line to your Podfile:

	pod 'Strainer'

# What it does

**StrainerLab** displays a list of country, and let search by string, group them by rank, set a scope..

All data and table updates are managed by Strainer

![StrainerVideo](https://j.gifs.com/0VjXQL.gif)

# Author

Tristan Leblanc <tristan@moosefactory.eu>

Twitter     :	<https://www.twitter.com/tristanleblanc>  
Google+     :	<https://plus.google.com/+TristanLeblanc>  

<https://itunes.apple.com/us/developer/moosefactory-software/id428441945>

***

# Contribute

If you wish to contribute, check the [CONTRIBUTE](CONTRIBUTE.md) file for more information.

***

# License

**Strainer** is available under the MIT license. See the [LICENSE](LICENSE) file for more info.

***

*31 May 2018* 

-
©2018 Tristan Leblanc **MooseFactory Software**